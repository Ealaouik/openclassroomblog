import { Component } from '@angular/core';
import {Post} from './model/post.model'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})




export class AppComponent {
 


  posts = [];
  
     
  constructor(){
    let titre1 = "Mon premier post";
    let content1 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
    let loveIts1 = 1;

    let post1 = new Post(titre1, content1,loveIts1);

    let titre2 = "Mon deuxième post";
    let content2 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
    let loveIts2 = -1;

    let post2 = new Post(titre2, content2,loveIts2);


    let titre3 = "Encore un post";
    let content3 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
    let loveIts3 = 0;

    let post3 = new Post(titre3, content3,loveIts3);

    this.posts=[post1,post2,post3];
  }
  
  
}
